---
layout: default
---

## Quickstart Guide

Click [here](https://bitbucket.org/UBCCS/jekyll-faculty/src/master/) to return to the 
repository website that shows instructions on how to use
and customize this UBC CS faculty and course static website generator yourself.  Browse
the markdown files in particular (```.md``` file extension; start with ```index.md``` -- make sure you view the raw source).

Browse the remainder of this demonstration website to gain a sense of what can
be easily and efficiently generated and maintained (like $x^2 + 1 = 17$, ```while(true) ++i```, and the references below).

---------------
## About Me

<img class="profile-picture" src="headshot.jpg">

I am a professor of Computer Science and Distinguished Scholar of the Sauder School of Business at the University of British Columbia.


## Research Interests

My goal is to integrate research in Artificial Intelligence, Human Computer Interaction and Cognitive Science to create intelligent user interfaces that can effectively and reliably adapt to the needs of each user. I am particularly interested in extending the range of user's states and traits that can be reliably captured in a computational user model and leveraged for adaptation - from purely cognitive features (knowledge, skills, goals), to affective states (emotions, moods, attitudes), to meta-cognitive skills (e.g., the capability of effectively exploring a large information space) and personality traits. I have over 100 peer-reviewed publications in the fields of Intelligent User Interfaces, User Modeling, Affective Computing, Intelligent Tutoring Systems, and Intelligent Virtual Agents. My research has received awards from a variety of venues, including UMUAI, the Journal of User Modeling and User Adapted Interaction (2002), the International Conference on Intelligent User Interfaces ( IUI 2007), the International Conference of User Modeling, Adaptation and Personalization (UMAP 2013, 2014), TiiS, ACM Transactions on Intelligent Interactive Systems (2014), and the International Conference on Intelligent Virtual Agents (IVA 2016). I am an associate editor for UMUAI, TiiS, IEEE Transactions on Affective Computing, and the Journal of Artificial Intelligence in Education. I served as President of AAAC, (Association for the Advancement of Affective Computing), as well as Program or Conference Chair for several international conferences including UMAP, IUI, and AI in Education

If you'd like to join our research group as a graduate student, please apply here

## Selected Publications

{% bibliography --file selected.bib %}
